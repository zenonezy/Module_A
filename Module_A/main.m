//
//  main.m
//  Module_A
//
//  Created by michael on 2018/5/4.
//  Copyright © 2018年 peiyou. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
